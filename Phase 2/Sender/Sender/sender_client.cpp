#include <winsock.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
#include <time.h>
#include <chrono>
#include <ctime>
#include <regex>
#include <vector>
#include <sstream>
#include <fstream>

#define HOSTNAME_LENGTH 20

#define ADDRESS_LENGTH 32 // Gmail: 320
#define SUBJECT_LENGTH 128 // Gmail: 998
#define BODY_LENGTH 512 // Gmail: 102400
#define EXTENSION_LENGTH 16
#define ATTACHMENT_LENGTH 256 * 1024 // Typical max: 20 * 1024 * 1024

#define RESP_LENGTH 64

#define BUFFER_LENGTH (ADDRESS_LENGTH * 2 + SUBJECT_LENGTH + sizeof(time_t) + BODY_LENGTH + EXTENSION_LENGTH + sizeof(unsigned int) + ATTACHMENT_LENGTH + 1) // old: 1024

#define REQUEST_PORT 5001
#define TRACE 0
#define MSGHDRSIZE 4 // message header size (4 bytes for length)

typedef enum ClientType {
	SENDER,
	RECEIVER
};

typedef struct Setup {
	char clienthostname[HOSTNAME_LENGTH];
	ClientType clienttype;
};

typedef struct Email {
	char toaddress[ADDRESS_LENGTH];
	char fromaddress[ADDRESS_LENGTH];
	char subject[SUBJECT_LENGTH];
	time_t timestamp;

	char body[BODY_LENGTH];

	char attachmentExtension[EXTENSION_LENGTH];
	unsigned int attachmentLength;
	char attachment[ATTACHMENT_LENGTH];
};  // email

typedef struct Conf {
	char response[RESP_LENGTH];
	time_t timestamp;
}; // confirmation

typedef struct Msg {
	unsigned int length; // length of effective bytes in the buffe
	char buffer[BUFFER_LENGTH];
}; // message format used for sending and receiving


class TcpClient {
	int sock;                    // socket descriptor
	struct sockaddr_in ServAddr; // server socket address
	unsigned short ServPort;     // server port
	Setup *setupp;				 // pointer to setup
	Email *emailp;               // pointer to request
	Conf *confp;                 // pointer to response
	Msg smsg, rmsg;              // receive_message and send_message
	WSADATA wsadata;

public:
	TcpClient() {}
	void run(int argc, char * argv[]);
	~TcpClient();
	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
	void err_sys(const char *fmt, ...);
};

/*
 * Email validation function, from https://stackoverflow.com/a/36904095
 */
bool is_email_valid(const std::string& email) {
	const std::regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
	return std::regex_match(email, pattern);
}

/*
 * Check if a file exists, from https://stackoverflow.com/a/12774387
 */
bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

/*
 * Load any file into vector, from https://stackoverflow.com/a/33942683
 */
void load_file_into_vector(const std::string &name, std::vector<char> &memblock) {
	std::streampos size;
	std::ifstream input(name, std::ios::in | std::ios::binary | std::ios::ate);

	if (input.is_open()) {
		size = input.tellg();
		memblock.resize(size);
		input.seekg(0, std::ios::beg);
		input.read(&memblock[0], size);
		input.close();
	}
}

void TcpClient::run(int argc, char *argv[]) {
	//
	// Initilize winsocket, proceed only if initialization succeeded
	//
	if (WSAStartup(0x0202, &wsadata) != 0) {
		WSACleanup();
		err_sys("Error in starting WSAStartup()\n");
	}


	//
	// Get the local machine hostname and display it, proceed only if could get the hostname
	//
	char clienthostname[HOSTNAME_LENGTH];
	if (gethostname(clienthostname, HOSTNAME_LENGTH) != 0) {
		err_sys("Error in getting local machine hostname\n");
	} else {
		std::cout << "Mail client starting on host: " << clienthostname << "\n";
	}

	std::string serverHostName;
	char serverhostname[HOSTNAME_LENGTH];
	std::cout << "Type name of mail server: ";
	std::getline(std::cin, serverHostName);
	std::cout << std::endl;
	strcpy_s(serverhostname, serverHostName.c_str());


	//
	// Create the socket, proceed only if socket creation succeeded
	//
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err_sys("Error in creating socket\n");
	}


	//
	// Connect to the server, proceed only if connected to the server
	//
	ServPort = REQUEST_PORT;
	memset(&ServAddr, 0, sizeof(ServAddr));  // zero out structure
	ServAddr.sin_family = AF_INET; // internet address family
	ServAddr.sin_addr.s_addr = ResolveName(serverhostname); // server IP address
	ServAddr.sin_port = htons(ServPort); // server port
	if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		err_sys("Error in connecting to the server\n");
	}


	//
	// Tell server that this is a Sender Client
	//
	Msg setupMsg;
	setupp = (Setup *)setupMsg.buffer;
	strcpy_s(setupp->clienthostname, clienthostname);
	setupp->clienttype = ClientType::SENDER;
	setupMsg.length = sizeof(Setup);

	std::cout << "Sending setup packet to the server..." << std::endl;
	if (msg_send(sock, &setupMsg) != sizeof(Setup)) {
		err_sys("Error in sending setup packet\n");
	} else {
		std::cout << "Successfully sent setup packet to the server" << std::endl << std::endl;
	}


	//
	// Construct the email
	//
	std::cout << "Creating new email..." << std::endl;

	emailp = (Email *)smsg.buffer; // Cast the send message to the request packet structure

	//std::cin.ignore(256, '\n');
	std::string to, from, subject, body, attachmentPath;

	do {
		std::cout << "TO: ";
		std::getline(std::cin, to);
	} while (!is_email_valid(to));
	strcpy_s(emailp->toaddress, to.c_str());

	do {
		std::cout << "FROM: ";
		std::getline(std::cin, from);
	} while (!is_email_valid(from));
	strcpy_s(emailp->fromaddress, from.c_str());

	std::cout << "SUBJECT: ";
	std::getline(std::cin, subject, '\n');

	strcpy_s(emailp->subject, subject.c_str());

	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	emailp->timestamp = end_time;

	std::cout << "BODY: ";
	std::getline(std::cin, body);
	strcpy_s(emailp->body, body.c_str());

	do {
		std::cout << "Attachment (enter empty string for no attachment): ";
		std::getline(std::cin, attachmentPath);
	} while (attachmentPath != "" && !file_exists(attachmentPath));
	if (!attachmentPath.empty()) {
		std::vector<char> memblock(0);
		load_file_into_vector(attachmentPath, memblock);
		std::copy(memblock.begin(), memblock.end(), emailp->attachment);
		strcpy_s(emailp->attachmentExtension, attachmentPath.substr(attachmentPath.find_last_of(".") + 1).c_str());
		emailp->attachmentLength = memblock.size();
	}


	//
	// Save email into local directory
	//
	std::stringstream ss;
	ss << end_time;
	std::string email_filename = subject + "_" + ss.str();


	std::cout << "Saving the email to \"" << email_filename << ".txt\"" << std::endl;
	std::ofstream fout;
	fout.open(email_filename + ".txt");
	if (!fout) {
		err_sys("Could not open a file to save email");
	}
	fout << "TO: " << to << std::endl;
	fout << "FROM: " << from << std::endl;
	fout << "SUBJECT: " << subject << std::endl;
	char end_time_str[32];
	ctime_s(end_time_str, sizeof(end_time_str), &end_time);
	fout << "TIMESTAMP: " << end_time_str;
	if (!attachmentPath.empty()) {
		fout << "ATTACHMENT PATH: " << attachmentPath << std::endl;
	}
	fout << "BODY: " << body << std::endl;
	fout.close();


	//
	// Send out the send message, proceed only if message was sent successfully
	//
	smsg.length = sizeof(Email);
	if (msg_send(sock, &smsg) != sizeof(Email)) {
		err_sys("Error in sending request packet\n");
	} else {
		std::cout << "\nMail Sent to Server, waiting...\n";
	}


	//
	// Receive the response message, proceed only if message was received successfully
	//
	if (msg_recv(sock, &rmsg) != rmsg.length) {
		err_sys("Error in receiving response message\n");
	}


	//
	// Cast the response message to the response packet structure
	//
	confp = (Conf *)rmsg.buffer;

	char conf_time_str[32];
	ctime_s(conf_time_str, sizeof(conf_time_str), &confp->timestamp);

	std::cout << "Server confirmation message at " << conf_time_str;
	std::cout << confp->response << std::endl;

	if (strcmp(confp->response, "250 OK") == 0) {
		printf("Email sent successfully\n");
	} else if (strcmp(confp->response, "501 Error") == 0) {
		printf("Email was not sent successfully\n");
	} else if (strcmp(confp->response, "550 Error") == 0) {
		printf("Email was not sent successfully\n");
	} else {
		printf("Unknown confirmation message\n");
	}


	//
	// Close the client socket
	//
	closesocket(sock);
}

TcpClient::~TcpClient() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}


void TcpClient::err_sys(const char * fmt, ...) { //from Richard Stevens's source code
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	system("pause");
	exit(1);
}

unsigned long TcpClient::ResolveName(char name[]) {
	struct hostent *host;            /* Structure containing host information */

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}

/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpClient::msg_recv(int sock, Msg * msg_ptr) {
	int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}

/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpClient::msg_send(int sock, Msg * msg_ptr) {
	int n;
	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n - MSGHDRSIZE);

}

int main(int argc, char *argv[]) { //argv[1]=servername argv[2]=filename argv[3]=time/size
	TcpClient *tc = new TcpClient();
	tc->run(argc, argv);

	//char sendEmail = 'N';
	//std::cout << "Do you want to send another email? (n: no, y: yes)";
	//std::cin >> sendEmail;
	//while (sendEmail == 'Y') {
	//	TcpClient *tc = new TcpClient();
	//	tc->run(argc, argv);
	//}

	system("pause");
	return 0;
}
