#ifndef SER_TCP_H
#define SER_TCP_H


#define HOSTNAME_LENGTH 20

#define ADDRESS_LENGTH 32 // Gmail: 320
#define SUBJECT_LENGTH 128 // Gmail: 998
#define BODY_LENGTH 512 // Gmail: 102400
#define EXTENSION_LENGTH 16
#define ATTACHMENT_LENGTH 256 * 1024 // Typical max: 20 * 1024 * 1024

#define RESP_LENGTH 64

#define BUFFER_LENGTH (ADDRESS_LENGTH * 2 + SUBJECT_LENGTH + sizeof(time_t) + BODY_LENGTH + EXTENSION_LENGTH + sizeof(unsigned int) + ATTACHMENT_LENGTH + 1) // old: 1024

#define REQUEST_PORT 5001
#define TRACE 0
#define MSGHDRSIZE 4 // message header size (4 bytes for length)
#define MAXPENDING 10

typedef enum ClientType {
	SENDER,
	RECEIVER
};

typedef struct Setup {
	char clienthostname[HOSTNAME_LENGTH];
	ClientType clienttype;
};

typedef struct Email {
	char toaddress[ADDRESS_LENGTH];
	char fromaddress[ADDRESS_LENGTH];
	char subject[SUBJECT_LENGTH];
	time_t timestamp;

	char body[BODY_LENGTH];

	char attachmentExtension[EXTENSION_LENGTH];
	unsigned int attachmentLength;
	char attachment[ATTACHMENT_LENGTH];
}; // email

typedef struct Conf {
	char response[RESP_LENGTH];
	time_t timestamp;
}; // confirmation

typedef struct Msg {
	unsigned int length; // length of effective bytes in the buffe
	char buffer[BUFFER_LENGTH];
}; // message format used for sending and receiving


Msg *sharedEmailMsgP = NULL;
bool emailSentSuccessfully = NULL;


class TcpServer {
	int serverSock;				      // socket descriptor for server
	int clientSock;					  // socket descriptor for client
	struct sockaddr_in ServerAddr;    // address of server
	struct sockaddr_in ClientAddr;    // address of client
	unsigned short ServerPort;        // server port
	int clientLen;				      // length of server address data structure
	char servername[HOSTNAME_LENGTH];

public:
	TcpServer();
	~TcpServer();
	void start();
};

class TcpThread : public Thread {
	int cs;

public:
	TcpThread(int clientsocket) :cs(clientsocket) {
	}
	virtual void run();
	virtual void runSender(const char *clienthostname);
	virtual void runReceiver(const char *clienthostname);

	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
	static void err_sys(const char * fmt, ...);
};

#endif