#include <winsock.h>
#include <iostream>
#include <windows.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include "Thread.h"
#include "server.h"
#include <regex>
#include <chrono>
#include <ctime>
#include <fstream>

/*
 * Email validation function, from https://stackoverflow.com/a/36904095
 */
bool is_email_valid(const std::string& email) {
	const std::regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
	return std::regex_match(email, pattern);
}

/*
* Save a vector into a file, from https://stackoverflow.com/a/14089675
*/
void save_file_from_vector(const std::string &name, const std::vector<char> &memblock) {
	std::ofstream fout(name, std::ios::out | std::ios::binary);
	fout.write((char*)&memblock[0], memblock.size());
	fout.close();
}

/*
 * Convert the reversed HEX IP address representation to regular string representation
 */
void hex_to_ipv4(ULONG &hex, std::string &address) {
	address =
		std::to_string(hex & 0x000000FF) + "." +
		std::to_string((hex & 0x0000FF00) >> 8) + "." +
		std::to_string((hex & 0x00FF0000) >> 16) + "." +
		std::to_string((hex & 0xFF000000) >> 24);
}

/*
 * Check if the email address exists in the mapping.in file
 */
bool mapping_exists(const std::string& email) {
	std::ifstream fin("mapping.in");
	std::string a, b;
	while (!fin.eof()) {
		fin >> a;
		fin >> b;
		if (a == email) {
			return true;
		}
	}
	return false;
}

/*
 * Get the hostname of a certain email address from the mapping.in file
 */
std::string get_host_name(const std::string& email) {
	std::ifstream fin("mapping.in");
	std::string a, b;
	while (!fin.eof()) {
		fin >> a;
		fin >> b;
		if (a == email) {
			fin.close();
			return b;
		}
	}
	fin.close();
	return "";
}





TcpServer::TcpServer() {
	WSADATA wsadata;
	if (WSAStartup(0x0202, &wsadata) != 0)
		TcpThread::err_sys("Starting WSAStartup() error\n");

	//Display name of local host
	if (gethostname(servername, HOSTNAME_LENGTH) != 0) //get the hostname
		TcpThread::err_sys("Get the host name error,exit");

	printf("Mail server Starting at host: %s\n", servername);
	printf("Waiting to be contacted for transferring Mail...\n\n");

	//Create the server socket
	if ((serverSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		TcpThread::err_sys("Create socket error,exit");

	//Fill-in Server Port and Address info.
	ServerPort = REQUEST_PORT;
	memset(&ServerAddr, 0, sizeof(ServerAddr));      /* Zero out structure */
	ServerAddr.sin_family = AF_INET;                 /* Internet address family */
	ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
	ServerAddr.sin_port = htons(ServerPort);         /* Local port */

	//Bind the server socket
	if (bind(serverSock, (struct sockaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
		TcpThread::err_sys("Bind socket error,exit");

	//Successfull bind, now listen for Server requests.
	if (listen(serverSock, MAXPENDING) < 0)
		TcpThread::err_sys("Listen socket error,exit");
}

TcpServer::~TcpServer() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}



void TcpServer::start() {
	while (true) {
		/* Set the size of the result-value parameter */
		clientLen = sizeof(ClientAddr);
		if (ClientAddr.sin_addr.S_un.S_addr != 0xcccccccc) {
			std::string ipaddress;
			hex_to_ipv4(ClientAddr.sin_addr.S_un.S_addr, ipaddress);
			std::cout << "Establishing connection with " << ipaddress << std::ends;
		}

		/* Wait for a Server to connect */
		if ((clientSock = accept(serverSock, (struct sockaddr *) &ClientAddr, &clientLen)) < 0) {
			TcpThread::err_sys("Accept Failed ,exit");
		}

		/* Create a thread for this new connection and run */
		TcpThread * pt = new TcpThread(clientSock);
		pt->start();
	}
}



//////////////////////////////TcpThread Class //////////////////////////////////////////
void TcpThread::err_sys(const char *fmt, ...) {
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	system("pause");
	exit(1);
}

unsigned long TcpThread::ResolveName(char name[]) {
	/* Structure containing host information */
	struct hostent *host;

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}


/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpThread::msg_recv(int sock, Msg * msg_ptr) {
	int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}


/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpThread::msg_send(int sock, Msg * msg_ptr) {
	int n;

	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");

	return (n - MSGHDRSIZE);
}



void TcpThread::run() {
	Setup *setupp; // pointer to setup packet
	Msg setupMsg; // setup message
	if (msg_recv(cs, &setupMsg) != setupMsg.length) {
		err_sys("Error in receiving setup message");
	}
	setupp = (Setup *)setupMsg.buffer;

	if (setupp->clienttype == ClientType::SENDER) {
		std::cout << "(Sender: " << setupp->clienthostname << ")" << std::endl;
		runSender(setupp->clienthostname);
	} else if (setupp->clienttype == ClientType::RECEIVER) {
		std::cout << "(Receiver: " << setupp->clienthostname << ")" << std::endl;
		runReceiver(setupp->clienthostname);
	}

	closesocket(cs);
}


void TcpThread::runSender(const char *clienthostname) {
	//
	// Receive the email message
	//
	std::cout << "Listening to sender for any email..." << std::endl;
	Msg emailMsg;
	if (msg_recv(cs, &emailMsg) != emailMsg.length) {
		err_sys("Receive Req error,exit");
	} else {
		std::cout << "Received an email from sender successfully" << std::endl << std::endl;
	}


	//
	// Construct the response
	//
	Email *emailp; //a pointer to the Request Packet
	emailp = (Email *)emailMsg.buffer;

	Msg confMsg;
	Conf *confp; //a pointer to response
	confMsg.length = sizeof(Conf);
	confp = (Conf *)confMsg.buffer;
	memset(confp->response, 0, sizeof(Conf));


	//
	// Validating header fields, and receiving client mapping
	// Constructing conf message
	//
	bool validEmail = false;
	std::cout << "Validating email header fields and receiving client mapping:" << std::endl;
	if (!is_email_valid(emailp->fromaddress) || !is_email_valid(emailp->toaddress)) {
		sprintf_s(confp->response, "501 Error");
		std::cout << "INVALID: Email addresses are not valid" << std::endl;

	} else if (!mapping_exists(emailp->toaddress)) {
		sprintf_s(confp->response, "550 Error");
		std::cout << "INVALID: TO email address does not exist in mapping" << std::endl;

	} else {
		sprintf_s(confp->response, "250 OK");
		std::cout << "VALID: Email is valid" << std::endl;
		validEmail = true;

	}

	auto current = std::chrono::system_clock::now();
	std::time_t current_time = std::chrono::system_clock::to_time_t(current);
	confp->timestamp = current_time;

	//
	// Pass email message to receiver thread
	// Only if 250 OK
	//
	if (validEmail) {
		sharedEmailMsgP = &emailMsg;

		while (emailSentSuccessfully == false);
	}


	//
	// Send conf message to sender
	//
	if (msg_send(cs, &confMsg) != confMsg.length) {
		err_sys("Sending confirmation message to sender has failed");
	}


	emailSentSuccessfully = false;
}

void TcpThread::runReceiver(const char *clienthostname) {
	std::cout << "Receiver is waiting to receive an email..." << std::endl;

	while (sharedEmailMsgP == NULL);

	//
	// Send email message to receiver
	//
	std::cout << "Sending the email to the receiver" << std::endl;
	if (msg_send(cs, sharedEmailMsgP) != sharedEmailMsgP->length) {
		emailSentSuccessfully = false;
		err_sys("Sending email message to receiver has failed");
	} else {
		std::cout << "Receiver has received the email successfully" << std::endl << std::endl;
	}
	emailSentSuccessfully = true;

	sharedEmailMsgP = NULL;
	sharedEmailMsgP = {};
}



////////////////////////////////////////////////////////////////////////////////////////

int main(void) {
	TcpServer ts;
	ts.start();

	system("pause");
	return 0;
}


