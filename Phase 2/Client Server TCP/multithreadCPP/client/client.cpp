#include <winsock.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
#include <time.h>
#include <chrono>
#include <ctime>

#define HOSTNAME_LENGTH 20
#define SUBJECT_LENGTH 20
#define TIMESTAMP_LENGTH 30
#define BODY_LENGTH 500
#define RESP_LENGTH 40
#define BUFFER_LENGTH 1024
#define REQUEST_PORT 5001
#define TRACE 0
#define MSGHDRSIZE 4 // message header size (4 bytes for length)

typedef struct {
	char clienthostname[HOSTNAME_LENGTH];

	char tohostname[HOSTNAME_LENGTH];
	char fromhostname[HOSTNAME_LENGTH];
	char subject[SUBJECT_LENGTH];
	char timestamp[TIMESTAMP_LENGTH];

	char body[BODY_LENGTH];
} Req;  //request

typedef struct {
	char response[RESP_LENGTH];
	char timestamp[TIMESTAMP_LENGTH];
} Resp; //response

typedef struct {
	int  length; // length of effective bytes in the buffe
	char buffer[BUFFER_LENGTH];
} Msg; // message format used for sending and receiving


class TcpClient {
	int sock;                    // socket descriptor
	struct sockaddr_in ServAddr; // server socket address
	unsigned short ServPort;     // server port
	Req *reqp;                   // pointer to request
	Resp *respp;                 // pointer to response
	Msg smsg, rmsg;              // receive_message and send_message
	WSADATA wsadata;

public:
	TcpClient() {}
	void run(int argc, char * argv[]);
	~TcpClient();
	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
	void err_sys(const char *fmt, ...);
};

void TcpClient::run(int argc, char *argv[]) {
	// Initilize winsocket, proceed only if initialization succeeded
	if (WSAStartup(0x0202, &wsadata) != 0) {
		WSACleanup();
		err_sys("Error in starting WSAStartup()\n");
	}

	// Cast the send message to the request packet structure
	reqp = (Req *)smsg.buffer;

	// Get the local machine hostname and display it, proceed only if could get the hostname
	if (gethostname(reqp->clienthostname, HOSTNAME_LENGTH) != 0) {
		err_sys("Error in getting local machine hostname\n");
	} else {
		std::cout << "Mail Client starting on host: " << reqp->clienthostname << "\n";
	}

	std::string serverHostName;
	char serverhostname[HOSTNAME_LENGTH];
	std::cout << "Type name of Mail server: ";
	std::getline(std::cin, serverHostName);
	strcpy_s(serverhostname, serverHostName.c_str());

	std::cout << "Creating New Email...\n";

	// Create the socket, proceed only if socket creation succeeded
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err_sys("Error in creating socket\n");
	}

	std::cin.ignore(256, '\n');
	std::string to, from, subject, body;
	std::cout << "\n";

	std::cout << "To: ";
	std::getline(std::cin, to);
	strcpy_s(reqp->tohostname, to.c_str());

	std::cout << "From: ";
	std::getline(std::cin, from);
	strcpy_s(reqp->fromhostname, from.c_str());

	std::cout << "Subject: ";
	std::getline(std::cin, subject);
	strcpy_s(reqp->subject, subject.c_str());

	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	char end_time_str[30];
	ctime_s(end_time_str, sizeof(end_time_str), &end_time);
	strcpy_s(reqp->timestamp, end_time_str);

	std::cout << "Body: ";
	std::getline(std::cin, body);
	strcpy_s(reqp->body, body.c_str());


	// Connect to the server, proceed only if connected to the server
	ServPort = REQUEST_PORT;
	memset(&ServAddr, 0, sizeof(ServAddr)); // zero out structure
	ServAddr.sin_family = AF_INET; // internet address family
	ServAddr.sin_addr.s_addr = ResolveName(serverhostname); // server IP address
	ServAddr.sin_port = htons(ServPort); // server port
	if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		err_sys("Error in connecting to the server\n");
	}

	// Send out the send message, proceed only if message was sent successfully
	smsg.length = sizeof(Req);
	if (msg_send(sock, &smsg) != sizeof(Req)) {
		err_sys("Error in sending request packet\n");
	} else {
		std::cout << "\nMail Sent to Server, waiting...\n";
	}

	// Receive the response message, proceed only if message was received successfully
	if (msg_recv(sock, &rmsg) != rmsg.length) {
		err_sys("Error in receiving response message\n");
	}

	// Cast the response message to the response packet structure
	respp = (Resp *)rmsg.buffer;
	if (strcmp(respp->response, "250 OK") == 0) {
		printf("Email received successfully at %s", respp->timestamp);
	} else if (strcmp(respp->response, "501 Error") == 0) {
		printf("Email was not received successfully at %s", respp->timestamp);
	}

	// Close the client socket
	closesocket(sock);
}

TcpClient::~TcpClient() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}


void TcpClient::err_sys(const char * fmt, ...) { //from Richard Stevens's source code
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(1);
}

unsigned long TcpClient::ResolveName(char name[]) {
	struct hostent *host;            /* Structure containing host information */

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}

/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpClient::msg_recv(int sock, Msg * msg_ptr) {
	int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}

/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpClient::msg_send(int sock, Msg * msg_ptr) {
	int n;
	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n - MSGHDRSIZE);

}

int main(int argc, char *argv[]) { //argv[1]=servername argv[2]=filename argv[3]=time/size
	TcpClient * tc = new TcpClient();
	tc->run(argc, argv);

	system("pause");
	return 0;
}
