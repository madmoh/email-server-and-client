#include <winsock.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
#include <time.h>
#include <chrono>
#include <ctime>
#include <fstream>
#include <sstream>
#include <vector>

#define HOSTNAME_LENGTH 20

#define ADDRESS_LENGTH 32 // Gmail: 320
#define SUBJECT_LENGTH 128 // Gmail: 998
#define BODY_LENGTH 512 // Gmail: 102400
#define EXTENSION_LENGTH 16
#define ATTACHMENT_LENGTH 256 * 1024 // Typical max: 20 * 1024 * 1024

#define RESP_LENGTH 64

#define BUFFER_LENGTH (ADDRESS_LENGTH * 2 + SUBJECT_LENGTH + sizeof(time_t) + BODY_LENGTH + EXTENSION_LENGTH + sizeof(unsigned int) + ATTACHMENT_LENGTH + 1) // old: 1024

#define REQUEST_PORT 5001
#define TRACE 0
#define MSGHDRSIZE 4 // message header size (4 bytes for length)

typedef enum ClientType {
	SENDER,
	RECEIVER
};

typedef struct Setup {
	char clienthostname[HOSTNAME_LENGTH];
	ClientType clienttype;
};

typedef struct Email {
	char toaddress[ADDRESS_LENGTH];
	char fromaddress[ADDRESS_LENGTH];
	char subject[SUBJECT_LENGTH];
	time_t timestamp;

	char body[BODY_LENGTH];

	char attachmentExtension[EXTENSION_LENGTH];
	unsigned int attachmentLength;
	char attachment[ATTACHMENT_LENGTH];
};  // email

typedef struct Conf {
	char response[RESP_LENGTH];
	time_t timestamp;
}; // confirmation

typedef struct Msg {
	unsigned int length; // length of effective bytes in the buffe
	char buffer[BUFFER_LENGTH];
}; // message format used for sending and receiving


class TcpClient {
	int sock;                    // socket descriptor
	struct sockaddr_in ServAddr; // server socket address
	unsigned short ServPort;     // server port
	Setup *setupp;				 // pointer to setup
	Email *emailp;               // pointer to email response
	Conf *confp;                 // pointer to confirmation response
	Msg emailMsg, confMsg;   // receive_message
	WSADATA wsadata;

public:
	TcpClient() {}
	void run(int argc, char * argv[]);
	~TcpClient();
	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
	void err_sys(const char *fmt, ...);
};


/*
* Save a vector into a file, from https://stackoverflow.com/a/14089675
*/
void save_file_from_vector(const std::string &name, const std::vector<char> &memblock) {
	std::ofstream fout(name, std::ios::out | std::ios::binary);
	fout.write((char*)&memblock[0], memblock.size());
	fout.close();
}


void TcpClient::run(int argc, char *argv[]) {
	//
	// Initilize winsocket, proceed only if initialization succeeded
	//
	if (WSAStartup(0x0202, &wsadata) != 0) {
		WSACleanup();
		err_sys("Error in starting WSAStartup()\n");
	}


	//
	// Get the local machine hostname and display it, proceed only if could get the hostname
	//
	char clienthostname[HOSTNAME_LENGTH];
	if (gethostname(clienthostname, HOSTNAME_LENGTH) != 0) {
		err_sys("Error in getting local machine hostname\n");
	} else {
		std::cout << "Mail client starting on host: " << clienthostname << "\n";
	}

	std::string serverHostName;
	char serverhostname[HOSTNAME_LENGTH];
	std::cout << "Type name of mail server: ";
	std::getline(std::cin, serverHostName);
	std::cout << std::endl;
	strcpy_s(serverhostname, serverHostName.c_str());


	//
	// Create the socket, proceed only if socket creation succeeded
	//
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err_sys("Error in creating socket\n");
	}


	//
	// Connect to the server, proceed only if connected to the server
	//
	ServPort = REQUEST_PORT;
	memset(&ServAddr, 0, sizeof(ServAddr)); // zero out structure
	ServAddr.sin_family = AF_INET; // internet address family
	ServAddr.sin_addr.s_addr = ResolveName(serverhostname); // server IP address
	ServAddr.sin_port = htons(ServPort); // server port
	if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		err_sys("Error in connecting to the server\n");
	}


	//
	// Tell server that this is a Receiver Client
	//
	Msg setupMsg;
	setupp = (Setup *)setupMsg.buffer;
	strcpy_s(setupp->clienthostname, clienthostname);
	setupp->clienttype = ClientType::RECEIVER;
	setupMsg.length = sizeof(Setup);

	std::cout << "Sending setup packet to the server..." << std::endl;
	if (msg_send(sock, &setupMsg) != sizeof(Setup)) {
		err_sys("Error in sending setup packet\n");
	} else {
		std::cout << "Successfully sent setup packet to the server" << std::endl << std::endl;
	}


	//
	// Receive the response message (email), proceed only if message was received successfully
	//
	std::cout << "Listening to server for any email..." << std::endl;
	if (msg_recv(sock, &emailMsg) != emailMsg.length) {
		err_sys("Error in receiving email message\n");
	} else {
		std::cout << "Received an email successfully" << std::endl << std::endl;
	}


	//
	// Cast the response message to the response packet structure
	//
	emailp = (Email *)emailMsg.buffer;


	//
	// Save email and attachment into a file
	//
	std::stringstream ss;
	ss << emailp->timestamp;
	std::string subject_string(emailp->subject);
	std::string email_filename = subject_string + "_" + ss.str();
	
	std::ofstream fout;
	fout.open(email_filename + ".txt");
	if (!fout) {
		err_sys("Could not open a file to save email");
	}
	fout << "TO: " << emailp->toaddress << std::endl;
	fout << "FROM: " << emailp->fromaddress << std::endl;
	fout << "SUBJECT: " << emailp->subject << std::endl;
	char end_time_str[32];
	ctime_s(end_time_str, sizeof(end_time_str), &emailp->timestamp);
	fout << "TIMESTAMP: " << end_time_str;
	if (emailp->attachmentLength > 0) {
		fout << "ATTACHMENT PATH: " << email_filename << "." << emailp->attachmentExtension << std::endl;

		std::vector<char> memblock(emailp->attachment, emailp->attachment + emailp->attachmentLength);
		save_file_from_vector(email_filename + "." + emailp->attachmentExtension, memblock);
	}
	fout << "BODY: " << emailp->body << std::endl;
	fout.close();
	std::cout << "Saved the email in \"" << email_filename << ".txt\"" << std::endl << std::endl;


	//
	// Display the email
	//
	std::cout << "Received the email headers and content: " << std::endl;
	std::cout << "TO: " << emailp->toaddress << std::endl;
	std::cout << "FROM: " << emailp->fromaddress << std::endl;
	std::cout << "SUBJECT: " << emailp->subject << std::endl;
	std::cout << "TIMESTAMP: " << end_time_str;
	if (emailp->attachmentLength > 0) {
		std::cout << "ATTACHMENT PATH: " << email_filename << "." << emailp->attachmentExtension << std::endl;
	}
	std::cout << "BODY: " << std::endl << emailp->body << std::endl;


	//
	// Close the client socket
	//
	closesocket(sock);
}

TcpClient::~TcpClient() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}


void TcpClient::err_sys(const char * fmt, ...) { //from Richard Stevens's source code
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	system("pause");
	exit(1);
}

unsigned long TcpClient::ResolveName(char name[]) {
	struct hostent *host;            /* Structure containing host information */

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}

/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpClient::msg_recv(int sock, Msg * msg_ptr) {
	int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}

/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpClient::msg_send(int sock, Msg * msg_ptr) {
	int n;
	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n - MSGHDRSIZE);

}

int main(int argc, char *argv[]) { //argv[1]=servername argv[2]=filename argv[3]=time/size
	TcpClient * tc = new TcpClient();
	tc->run(argc, argv);

	system("pause");
	return 0;
}
