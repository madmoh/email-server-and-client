#include <regex>
#include <fstream>
#include <sstream>
#include <vector>
#include <windows.h>
#include <iostream>
#include <filesystem>

#include "data_structures.cpp"

static void err_sys(const char *fmt, ...) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	system("pause");
	exit(1);
}

/*
 * Email validation function, from https://stackoverflow.com/a/36904095
 */
static bool is_email_valid(const std::string& email) {
	const std::regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
	return std::regex_match(email, pattern);
}


static std::vector<std::string> get_emails(const std::string& str) {


	std::string email;
	std::stringstream ss(str);

	std::vector<std::string> emails;

	if (str.find(';') == -1) {
		emails.push_back(str);
	} else {
		while (getline(ss, email, ';')) {
			emails.push_back(email);
		}
	}

	return emails;
}

static bool are_emails_valid(const std::string& str) {
	std::vector<std::string> emails = get_emails(str);
	for (std::string email : emails) {
		if (!is_email_valid(email)) {
			return false;
		}
	}
	return true;
}

/*
 * Check if a file exists, from https://stackoverflow.com/a/12774387
 */
static bool file_exists(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

/*
 * Load any file into vector, from https://stackoverflow.com/a/33942683
 */
static void load_file_into_vector(const std::string &name, std::vector<char> &memblock) {
	std::streampos size;
	std::ifstream input(name, std::ios::in | std::ios::binary | std::ios::ate);

	if (input.is_open()) {
		size = input.tellg();
		memblock.resize(size);
		input.seekg(0, std::ios::beg);
		input.read(&memblock[0], size);
		input.close();
	}
}

/*
* Save a vector into a file, from https://stackoverflow.com/a/14089675
*/
static void save_file_from_vector(const std::string &name, const std::vector<char> &memblock) {
	std::ofstream fout(name, std::ios::out | std::ios::binary);
	fout.write((char*)&memblock[0], memblock.size());
	fout.close();
}

/*
 * Convert the reversed HEX IP address representation to regular string representation
 */
static void hex_to_ipv4(ULONG &hex, std::string &address) {
	address =
		std::to_string(hex & 0x000000FF) + "." +
		std::to_string((hex & 0x0000FF00) >> 8) + "." +
		std::to_string((hex & 0x00FF0000) >> 16) + "." +
		std::to_string((hex & 0xFF000000) >> 24);
}

/*
	* Get email filename with no extension
	*/
static std::string get_email_filename(Email *emailp) {
	std::stringstream ss;
	ss << emailp->timestamp;
	std::string subject_string(emailp->subject);
	std::string email_filename = subject_string + "_" + ss.str();

	return email_filename;
}

static time_t get_last_email_time(std::string path) {
	std::vector<unsigned int> times;

	for (const auto & entry : std::filesystem::directory_iterator(path)) {
		std::string p = entry.path().string();

		int lastDot = p.find_last_of('.');
		if (lastDot > -1) {
			p = p.substr(0, lastDot);
		}

		int lastUS = p.find_last_of('_');
		p = p.substr(lastUS + 1);

		times.push_back(std::stoi(p));
	}

	if (times.size() > 0) {
		return *std::max_element(times.begin(), times.end());
	} else {
		return 0;
	}
}


/*
* Save email into a file
*/
static void save_email(std::string path, Email *emailp, Attachment *attachmentp) {
	std::string email_filename = get_email_filename(emailp);

	std::ofstream fout;
	fout.open(path + "\\" + email_filename + ".txt");
	if (!fout) {
		err_sys("Could not open a file to save email");
	}
	fout << "TO: " << emailp->toaddresses << std::endl;
	fout << "FROM: " << emailp->fromaddress << std::endl;
	fout << "SUBJECT: " << emailp->subject << std::endl;
	fout << "TIMESTAMP: " << emailp->timestamp << std::endl;
	if (emailp->hasAttachment) {
		fout << "ATTACHMENT PATH: " << email_filename << "." << attachmentp->attachmentExtension << std::endl;
	}
	fout << "BODY: " << emailp->body << std::endl;
	fout.close();
	std::cout << "Saved the email in \"" << email_filename << ".txt\"" << std::endl << std::endl;
}

static std::string get_email(std::string path, Email *emailp, Attachment *attachmentp) {
	std::ifstream fin(path);

	std::string line;
	std::string attachmentPath;

	while (std::getline(fin, line)) {
		std::string input = line.substr(0, line.find_first_of(':') + 1);
		if (input == "TO:") {
			strcpy_s(emailp->toaddresses, line.substr(line.find_first_of(':') + 2).c_str());
		} else if (input == "FROM:") {
			strcpy_s(emailp->fromaddress, line.substr(line.find_first_of(':') + 2).c_str());
		} else if (input == "SUBJECT:") {
			strcpy_s(emailp->subject, line.substr(line.find_first_of(':') + 2).c_str());
		} else if (input == "TIMESTAMP:") {
			emailp->timestamp = std::stoi(line.substr(line.find_first_of(':') + 2));
		} else if (input == "ATTACHMENT PATH:") {
			emailp->hasAttachment = true;
			strcpy_s(attachmentp->attachmentExtension, line.substr(line.find_last_of('.') + 2).c_str());
			attachmentPath = line.substr(line.find_first_of(':') + 2);
		} else if (input == "BODY:") {
			strcpy_s(emailp->body, line.substr(line.find_first_of(':') + 2).c_str());
		}
	}

	fin.close();

	return attachmentPath;
}

static void print_folder(std::string path, std::string type) {
	std::cout << type << std::endl;
	fprintf(stdout, " ----------------------------------------------------------------------------------- \n");
	fprintf(stdout, "|%-20.20s|%-20.20s|%-20.20s|%-20.20s|\n",
		"From",
		"To",
		"Subject",
		"Timestamp");
	fprintf(stdout, "-------------------------------------------------------------------------------------\n");
	std::string folder = path;
	for (const auto & entry : std::filesystem::directory_iterator(folder)) {
		std::string line = entry.path().string();
		if (line.substr(line.size() - 4) == ".txt") {
			Msg emailMsg;
			emailMsg.length = sizeof(Email);
			Email *emailp = (Email *)emailMsg.buffer;

			Msg attachmentMsg;
			attachmentMsg.length = sizeof(Attachment);
			Attachment *attachmentp = (Attachment *)attachmentMsg.buffer;

			get_email(line, emailp, attachmentp);

			char end_time_str[32];
			ctime_s(end_time_str, sizeof(end_time_str), &emailp->timestamp);

			fprintf(stdout, "|%-20.20s|%-20.20s|%-20.20s|%-20.20s|\n",
				emailp->fromaddress, emailp->toaddresses, emailp->subject, end_time_str);
		}
	}
	fprintf(stdout, " ----------------------------------------------------------------------------------- \n\n");
}