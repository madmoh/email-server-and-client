#define HOSTNAME_LENGTH 20

#define ADDRESS_LENGTH 32 // Gmail: 320
#define SUBJECT_LENGTH 128 // Gmail: 998
#define BODY_LENGTH 512 // Gmail: 102400
#define EXTENSION_LENGTH 16
#define ATTACHMENT_LENGTH 64 * 1024 // Typical max: 20 * 1024 * 1024

#define RESP_LENGTH 64

#define BUFFER_LENGTH (ADDRESS_LENGTH * 2 + SUBJECT_LENGTH + sizeof(time_t) + BODY_LENGTH + EXTENSION_LENGTH + sizeof(unsigned int) + ATTACHMENT_LENGTH + 1) // old: 1024

#define REQUEST_PORT 5001
#define TRACE 0
#define MSGHDRSIZE 4 // message header size (4 bytes for length)
#define MAXPENDING 10