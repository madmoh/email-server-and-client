#include <time.h>
#include "constants.cpp"

enum ClientType {
	SENDER,
	RECEIVER
};

// Setup size: 24
struct Setup {
	char clienthostname[HOSTNAME_LENGTH];
	ClientType clienttype;
};

struct LastInbox {
	time_t lastinbox;
};

struct LastInboxResponse {
	unsigned int newemails;
};

// Email size: 1006
struct Email {
	char toaddresses[ADDRESS_LENGTH * 10 + 9]; // maximum 10 TOs, with 9 ';' between each two
	char fromaddress[ADDRESS_LENGTH];
	char subject[SUBJECT_LENGTH];
	time_t timestamp;

	char body[BODY_LENGTH];

	bool hasAttachment;
};

// Attachment size: 65,556
struct Attachment {
	unsigned int size;
	char attachmentExtension[EXTENSION_LENGTH];
	char data[ATTACHMENT_LENGTH];
};

// Conf size: 68
struct Conf {
	char response[RESP_LENGTH];
	time_t timestamp;
};

struct Msg {
	unsigned int length; // length of effective bytes in the buffe
	char buffer[sizeof(Attachment)];
	//char *buffer;
}; // message format used for sending and receiving