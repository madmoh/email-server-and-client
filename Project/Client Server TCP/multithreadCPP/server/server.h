#ifndef SER_TCP_H
#define SER_TCP_H

#include "../clientserver/functions.cpp"


class TcpServer {
	int serverSock;				      // socket descriptor for server
	int clientSock;					  // socket descriptor for client
	struct sockaddr_in ServerAddr;    // address of server
	struct sockaddr_in ClientAddr;    // address of client
	unsigned short ServerPort;        // server port
	int clientLen;				      // length of server address data structure
	char servername[HOSTNAME_LENGTH];

public:
	TcpServer();
	~TcpServer();
	void start();
};

class TcpThread : public Thread {
	int cs;

public:
	TcpThread(int clientsocket) :cs(clientsocket) {
	}
	virtual void run();
	virtual void runSender(const char *clienthostname);
	virtual void runReceiver(const char *clienthostname);

	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
};

#endif