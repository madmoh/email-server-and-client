#include <winsock.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include "Thread.h"
#include "server.h"
#include <chrono>
#include <ctime>


/*
 * Check if the email address exists in the mapping.in file
 */
bool mapping_exists(const std::string& str) {
	std::vector<std::string> emails = get_emails(str);

	for (std::string email : emails) {
		std::ifstream fin("mapping.in");
		std::string a, b;
		bool thisExist = false;
		while (!fin.eof()) {
			fin >> a;
			fin >> b;
			if (a == email) {
				thisExist = true;
				break;
			}
		}
		if (thisExist) {
			continue;
		}
		fin.close();
		return false;
	}
	return true;
}

/*
 * Check if the email address and host name are alligned
 */
bool verify_mapping(const std::string& email, const std::string& host) {
	std::ifstream fin("mapping.in");
	std::string a, b;
	while (!fin.eof()) {
		fin >> a;
		fin >> b;
		if (a == email && b == host) {
			return true;
		}
	}
	return false;
}

/*
 * Get the hostname of a certain email address from the mapping.in file
 */
std::string get_host_name(const std::string& email) {
	std::ifstream fin("mapping.in");
	std::string a, b;
	while (!fin.eof()) {
		fin >> a;
		fin >> b;
		if (a == email) {
			fin.close();
			return b;
		}
	}
	fin.close();
	return "";
}

/*
 * Get the email address of a certain hostname from the mapping.in file
 */
std::string get_email_address(const std::string& hostname) {
	std::ifstream fin("mapping.in");
	std::string a, b;
	while (!fin.eof()) {
		fin >> a;
		fin >> b;
		if (b == hostname) {
			fin.close();
			return a;
		}
	}
	fin.close();
	return "";
}



TcpServer::TcpServer() {
	WSADATA wsadata;
	if (WSAStartup(0x0202, &wsadata) != 0)
		err_sys("Starting WSAStartup() error\n");

	//Display name of local host
	if (gethostname(servername, HOSTNAME_LENGTH) != 0) //get the hostname
		err_sys("Get the host name error,exit");

	printf("Mail server starting at host: %s\n", servername);
	printf("Waiting to be contacted for transferring mail...\n\n");

	//Create the server socket
	if ((serverSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		err_sys("Create socket error,exit");

	//Fill-in Server Port and Address info.
	ServerPort = REQUEST_PORT;
	memset(&ServerAddr, 0, sizeof(ServerAddr));      /* Zero out structure */
	ServerAddr.sin_family = AF_INET;                 /* Internet address family */
	ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
	ServerAddr.sin_port = htons(ServerPort);         /* Local port */

	//Bind the server socket
	if (bind(serverSock, (struct sockaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
		err_sys("Bind socket error,exit");

	//Successfull bind, now listen for Server requests.
	if (listen(serverSock, MAXPENDING) < 0)
		err_sys("Listen socket error,exit");
}

TcpServer::~TcpServer() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}



void TcpServer::start() {
	while (true) {
		/* Set the size of the result-value parameter */
		clientLen = sizeof(ClientAddr);
		if (ClientAddr.sin_addr.S_un.S_addr != 0xcccccccc) {
			std::string ipaddress;
			hex_to_ipv4(ClientAddr.sin_addr.S_un.S_addr, ipaddress);
			std::cout << "Establishing connection with " << ipaddress << std::ends;
		}

		/* Wait for a Server to connect */
		if ((clientSock = accept(serverSock, (struct sockaddr *) &ClientAddr, &clientLen)) < 0) {
			err_sys("Accept Failed ,exit");
		}

		/* Create a thread for this new connection and run */
		TcpThread * pt = new TcpThread(clientSock);
		pt->start();
	}
}

//////////////////////////////TcpThread Class //////////////////////////////////////////
unsigned long TcpThread::ResolveName(char name[]) {
	/* Structure containing host information */
	struct hostent *host;

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}


/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpThread::msg_recv(int sock, Msg * msg_ptr) {
	unsigned int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}


/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpThread::msg_send(int sock, Msg * msg_ptr) {
	int n;

	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");

	return (n - MSGHDRSIZE);
}



void TcpThread::run() {
	Msg setupMsg; // setup message
	//setupMsg.buffer = new char[sizeof(Setup)];
	if (msg_recv(cs, &setupMsg) != setupMsg.length) {
		err_sys("Error in receiving setup message");
	}

	Setup *setupp; // pointer to setup packet
	setupp = (Setup *)setupMsg.buffer;

	if (setupp->clienttype == ClientType::SENDER) {
		std::cout << "(Sender: " << setupp->clienthostname << ")" << std::endl;
		runSender(setupp->clienthostname);
	} else if (setupp->clienttype == ClientType::RECEIVER) {
		std::cout << "(Receiver: " << setupp->clienthostname << ")" << std::endl;
		runReceiver(setupp->clienthostname);
	}

	closesocket(cs);
}


void TcpThread::runSender(const char *clienthostname) {
	// Receive the email message
	std::cout << "Listening to sender for any email..." << std::endl;
	Msg emailMsg;
	Email *emailp = (Email *)emailMsg.buffer;

	if (msg_recv(cs, &emailMsg) != emailMsg.length) {
		err_sys("Receive Req error,exit");
	} else {
		std::cout << "Received an email from sender successfully" << std::endl << std::endl;
	}

	std::vector<std::string> emails = get_emails(emailp->toaddresses);

	Msg attachmentMsg;
	Attachment *attachmentp = (Attachment *)attachmentMsg.buffer;

	// Construct the response
	Msg confMsg;
	Conf *confp = (Conf *)confMsg.buffer;
	confMsg.length = sizeof(Conf);
	memset(confp->response, 0, RESP_LENGTH);

	// Validating header fields, and receiving client mapping
	bool validEmail = false;
	if (!is_email_valid(emailp->fromaddress) || !are_emails_valid(emailp->toaddresses)) {
		sprintf_s(confp->response, "501 Error");
		std::cout << "INVALID: Email addresses are not valid" << std::endl;

	} else if (!mapping_exists(emailp->toaddresses)) {
		sprintf_s(confp->response, "550 Error");
		std::cout << "INVALID: TO email address does not exist in mapping" << std::endl;

	} else if (!verify_mapping(emailp->fromaddress, clienthostname)) {
		sprintf_s(confp->response, "550 Error");
		std::cout << "INVALID: FROM email address does not match host" << std::endl;

	} else if (!mapping_exists(emailp->fromaddress)) {
		sprintf_s(confp->response, "550 Error");
		std::cout << "INVALID: FROM email address does not exist in mapping" << std::endl;

	} else {
		sprintf_s(confp->response, "250 OK");
		std::cout << "VALID: Email is valid" << std::endl;
		validEmail = true;
	}

	//
	// Step 5: Server saves the email to client 2's inbox directory
	// Step 6: Server saves the email to client 1's sent directory
	//
	if (validEmail) {
		if (emailp->hasAttachment) {
			unsigned int tempsize = 0;
			int chunksize = 0;

			do {
				if (msg_recv(cs, &attachmentMsg) != attachmentMsg.length) {
					err_sys("Receive Req error,exit");
				}

				tempsize += ATTACHMENT_LENGTH;
				chunksize = tempsize > attachmentp->size ? attachmentp->size - tempsize + ATTACHMENT_LENGTH : ATTACHMENT_LENGTH;

				for (std::string email : emails) {
					std::ofstream frecout((std::string)("Data\\") + email + "\\inbox\\" + get_email_filename(emailp) + "." + attachmentp->attachmentExtension,
						std::ios::out | std::ios::binary | std::ios::app);
					frecout.write((char*)&attachmentp->data[0], chunksize);
					frecout.close();
				}



				std::ofstream fsenout((std::string)("Data\\") + emailp->fromaddress + "\\sent\\" + get_email_filename(emailp) + "." + attachmentp->attachmentExtension,
					std::ios::out | std::ios::binary | std::ios::app);
				fsenout.write((char*)&attachmentp->data[0], chunksize);
				fsenout.close();
			} while (tempsize < attachmentp->size);

		}

		for (std::string email : emails) {
			save_email((std::string)("Data\\") + email + "\\inbox", emailp, attachmentp);
		}
		save_email((std::string)("Data\\") + emailp->fromaddress + "\\sent", emailp, attachmentp);
	}

	auto current = std::chrono::system_clock::now();
	std::time_t current_time = std::chrono::system_clock::to_time_t(current);
	confp->timestamp = current_time;

	// Send conf message to sender
	if (msg_send(cs, &confMsg) != confMsg.length) {
		err_sys("Sending confirmation message to sender has failed");
	}
}

void TcpThread::runReceiver(const char *clienthostname) {
	Msg lastInboxMsg;
	LastInbox *lastinboxp = (LastInbox *)lastInboxMsg.buffer;
	if (msg_recv(cs, &lastInboxMsg) != lastInboxMsg.length) {
		err_sys("Receive Req error,exit");
	}


	//
	// Step 9: Server sends client 2 a copy of inbox directory
	//
	std::vector<std::string> emailsToSend;

	std::string address = get_email_address((std::string)clienthostname);
	std::string path = (std::string)("Data\\") + address + "\\inbox";

	for (const auto & entry : std::filesystem::directory_iterator(path)) {
		std::string line = entry.path().string(), p = entry.path().string();

		int lastDot = p.find_last_of(".txt");
		if (lastDot > -1) {
			p = p.substr(0, lastDot);
		}

		int lastUS = p.find_last_of('_');
		p = p.substr(lastUS + 1);

		int pint = std::stoi(p);

		if (pint > lastinboxp->lastinbox && line.substr(line.size() - 4) == ".txt") {
			emailsToSend.push_back(entry.path().string());
		}
	}

	Msg lastInboxResponseMsg;
	lastInboxResponseMsg.length = sizeof(LastInboxResponse);
	LastInboxResponse *lastinboxresponsep = (LastInboxResponse *)lastInboxResponseMsg.buffer;

	lastinboxresponsep->newemails = emailsToSend.size();

	if (msg_send(cs, &lastInboxResponseMsg) != sizeof(LastInboxResponse)) {
		err_sys("Error in sending setup packet\n");
	}

	for (int i = 0; i < emailsToSend.size(); i++) {
		Msg emailMsg;
		emailMsg.length = sizeof(Email);
		Email *emailp = (Email *)emailMsg.buffer;

		Msg attachmentMsg;
		attachmentMsg.length = sizeof(Attachment);
		Attachment *attachmentp = (Attachment *)attachmentMsg.buffer;

		std::string attachmentPath = get_email(emailsToSend[i], emailp, attachmentp);

		if (msg_send(cs, &emailMsg) != sizeof(Email)) {
			err_sys("Error in sending request packet\n");
		}

		if (emailp->hasAttachment) {
			std::ifstream fin(path + "\\" + attachmentPath, std::ifstream::binary);
			std::vector<char> x(ATTACHMENT_LENGTH, 0);

			attachmentp = (Attachment *)attachmentMsg.buffer;

			strcpy_s(attachmentp->attachmentExtension, attachmentPath.substr(attachmentPath.find_last_of(".") + 1).c_str());

			auto begin = fin.tellg();
			fin.seekg(0, std::ios::end);
			auto end = fin.tellg();
			auto fsize = (end - begin);
			attachmentp->size = fsize;
			fin.seekg(0, std::ios::beg);

			unsigned int tempsize = 0;

			do {
				fin.read(x.data(), x.size());
				memcpy_s(attachmentp->data, ATTACHMENT_LENGTH, x.data(), ATTACHMENT_LENGTH);

				if (msg_send(cs, &attachmentMsg) != sizeof(Attachment)) {
					err_sys("Error in sending request packet\n");
				}

				tempsize += ATTACHMENT_LENGTH;
			} while (tempsize < attachmentp->size);
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////

int main(void) {
	TcpServer ts;
	ts.start();

	system("pause");
	return 0;
}


