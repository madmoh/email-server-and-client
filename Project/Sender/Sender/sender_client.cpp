#include <winsock.h>
#include <stdio.h>
#include <string>
#include <windows.h>
#include <chrono>
#include <ctime>

#include "../../Client Server TCP/multithreadCPP/clientserver/functions.cpp"

class TcpClient {
	int sock;                    // socket descriptor
	struct sockaddr_in ServAddr; // server socket address
	unsigned short ServPort;     // server port
	WSADATA wsadata;

public:
	TcpClient() {}
	void runSender();
	void runReceiver();
	~TcpClient();
	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
};


void TcpClient::runSender() {
	// Initilize winsocket
	if (WSAStartup(0x0202, &wsadata) != 0) {
		WSACleanup();
		err_sys("Error in starting WSAStartup()\n");
	}


	// Get the local machine hostname and display it
	char clienthostname[HOSTNAME_LENGTH];
	if (gethostname(clienthostname, HOSTNAME_LENGTH) != 0) {
		err_sys("Error in getting local machine hostname\n");
	} else {
		std::cout << "Mail client starting on host: " << clienthostname << "\n";
	}


	//
	// Step 2: Client 1 constructs email to send to client 2 with an attachment
	//
	Msg emailMsg;
	emailMsg.length = sizeof(Email);
	Email *emailp = (Email *)emailMsg.buffer;

	std::cin.ignore(1, '\n');
	std::string to, from, subject, body, attachmentPath;

	do {
		std::cout << "FROM: ";
		std::getline(std::cin, from);
	} while (!is_email_valid(from));
	strcpy_s(emailp->fromaddress, from.c_str());

	do {
		std::cout << "TO: ";
		std::getline(std::cin, to);
	} while (!are_emails_valid(to));
	strcpy_s(emailp->toaddresses, to.c_str());

	std::cout << "SUBJECT: ";
	std::getline(std::cin, subject, '\n');

	strcpy_s(emailp->subject, subject.c_str());

	auto end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	emailp->timestamp = end_time;

	std::cout << "BODY: ";
	std::getline(std::cin, body);
	strcpy_s(emailp->body, body.c_str());

	do {
		std::cout << "Attachment (leave blank for no attachment): ";
		std::getline(std::cin, attachmentPath);
	} while (attachmentPath != "" && !file_exists(attachmentPath));
	emailp->hasAttachment = !attachmentPath.empty();
	std::cout << std::endl;


	//
	// Step 3: Client 1 connects to mail server
	//
	std::string serverHostName;
	char serverhostname[HOSTNAME_LENGTH];
	do {
		std::cout << "Type name of mail server: ";
		std::getline(std::cin, serverHostName);
	} while (serverHostName.empty());
	strcpy_s(serverhostname, serverHostName.c_str());

	std::cout << std::endl;

	// Create the socket
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err_sys("Error in creating socket\n");
	}

	// Connect to the server
	ServPort = REQUEST_PORT;
	memset(&ServAddr, 0, sizeof(ServAddr));  // zero out structure
	ServAddr.sin_family = AF_INET; // internet address family
	ServAddr.sin_addr.s_addr = ResolveName(serverhostname); // server IP address
	ServAddr.sin_port = htons(ServPort); // server port
	if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		err_sys("Error in connecting to the server\n");
	}

	// Tell server that this is a Sender Client
	Msg setupMsg;
	Setup *setupp = (Setup *)setupMsg.buffer;
	setupMsg.length = sizeof(Setup);

	strcpy_s(setupp->clienthostname, clienthostname);
	setupp->clienttype = ClientType::SENDER;

	if (msg_send(sock, &setupMsg) != sizeof(Setup)) {
		err_sys("Error in sending setup packet\n");
	}


	//
	// Step 4: Client 1 sends email to the server
	//
	if (msg_send(sock, &emailMsg) != sizeof(Email)) {
		err_sys("Error in sending request packet\n");
	}

	Msg attachmentMsg;
	attachmentMsg.length = sizeof(Attachment);
	Attachment *attachmentp = (Attachment *)attachmentMsg.buffer;;

	if (!attachmentPath.empty()) {
		std::ifstream fin(attachmentPath, std::ifstream::binary);
		std::vector<char> x(ATTACHMENT_LENGTH, 0);

		strcpy_s(attachmentp->attachmentExtension, attachmentPath.substr(attachmentPath.find_last_of(".") + 1).c_str());

		auto begin = fin.tellg();
		fin.seekg(0, std::ios::end);
		auto end = fin.tellg();
		auto fsize = (end - begin);
		attachmentp->size = fsize;
		fin.seekg(0, std::ios::beg);

		unsigned int tempsize = 0;

		do {
			fin.read(x.data(), x.size());
			memcpy_s(attachmentp->data, ATTACHMENT_LENGTH, x.data(), ATTACHMENT_LENGTH);

			if (msg_send(sock, &attachmentMsg) != sizeof(Attachment)) {
				err_sys("Error in sending request packet\n");
			}

			tempsize += ATTACHMENT_LENGTH;
		} while (tempsize < attachmentp->size);

		std::ifstream  src(attachmentPath, std::ios::binary);
		std::ofstream  dst((std::string)("Data\\") + "sent\\" + get_email_filename(emailp) + "." + attachmentp->attachmentExtension,
			std::ios::binary);
		dst << src.rdbuf();
	}



	//
	// Specification 10: Client 2 receives the response message
	//
	Msg confMsg;
	Conf *confp = (Conf *)confMsg.buffer;
	if (msg_recv(sock, &confMsg) != confMsg.length) {
		err_sys("Error in receiving response message\n");
	}

	char conf_time_str[32];
	ctime_s(conf_time_str, sizeof(conf_time_str), &confp->timestamp);

	std::cout << "Server confirmation message at " << conf_time_str;

	if (strcmp(confp->response, "250 OK") == 0) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
		printf("SERVER: Email sent successfully\n");
		save_email((std::string)("Data\\") + "sent", emailp, attachmentp);

	} else if (strcmp(confp->response, "501 Error") == 0) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
		printf("SERVER: 501 Error: Email was not sent successfully.\n");
		printf("\tEmail addresses are not valid.\n");

	} else if (strcmp(confp->response, "550 Error") == 0) {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
		printf("SERVER: 551 Error: Email was not sent successfully. Could be one of the following:\n");
		printf("\tFROM or TO email address is not registered\n");
		printf("\tFROM email address does not match host\n");

	} else {
		HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); SetConsoleTextAttribute(hConsole, FOREGROUND_RED);

		printf("Unknown confirmation message\n");
	}
	std::cout << std::endl;


	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_BLUE);

	// Close the client socket
	closesocket(sock);
}

void TcpClient::runReceiver() {
	// Initilize winsocket
	if (WSAStartup(0x0202, &wsadata) != 0) {
		WSACleanup();
		err_sys("Error in starting WSAStartup()\n");
	}

	// Get the local machine hostname and display it
	char clienthostname[HOSTNAME_LENGTH];
	if (gethostname(clienthostname, HOSTNAME_LENGTH) != 0) {
		err_sys("Error in getting local machine hostname\n");
	} else {
		std::cout << "Mail client starting on host: " << clienthostname << "\n";
	}

	// Create the socket
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err_sys("Error in creating socket\n");
	}


	//
	// Step 7: Client 2 connects to the mail server
	//
	std::cin.ignore(1, '\n');
	std::cin.clear(); std::cin.sync();
	std::string serverHostName;
	char serverhostname[HOSTNAME_LENGTH];
	std::cout << "Type name of mail server: ";
	std::getline(std::cin, serverHostName);
	std::cout << std::endl;
	strcpy_s(serverhostname, serverHostName.c_str());

	// Connect to the server
	ServPort = REQUEST_PORT;
	memset(&ServAddr, 0, sizeof(ServAddr)); // zero out structure
	ServAddr.sin_family = AF_INET; // internet address family
	ServAddr.sin_addr.s_addr = ResolveName(serverhostname); // server IP address
	ServAddr.sin_port = htons(ServPort); // server port
	if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		err_sys("Error in connecting to the server\n");
	}

	// Tell server that this is a Receiver Client
	Msg setupMsg;
	setupMsg.length = sizeof(Setup);
	Setup *setupp = (Setup *)setupMsg.buffer;

	strcpy_s(setupp->clienthostname, clienthostname);
	setupp->clienttype = ClientType::RECEIVER;

	if (msg_send(sock, &setupMsg) != sizeof(Setup)) {
		err_sys("Error in sending setup packet\n");
	}


	//
	// Step 8: Client 2 requests updating local "inbox" directory
	//
	Msg lastInboxMsg;
	lastInboxMsg.length = sizeof(LastInbox);
	LastInbox *lastinboxp = (LastInbox *)lastInboxMsg.buffer;

	lastinboxp->lastinbox = get_last_email_time((std::string)("Data\\") + "inbox\\");

	if (msg_send(sock, &lastInboxMsg) != sizeof(LastInbox)) {
		err_sys("Error in sending setup packet\n");
	}

	// Learn how many new emails there is
	Msg lastInboxResponseMsg;
	LastInboxResponse *lastinboxresponsep = (LastInboxResponse *)lastInboxResponseMsg.buffer;

	if (msg_recv(sock, &lastInboxResponseMsg) != lastInboxResponseMsg.length) {
		err_sys("Error in receiving email message\n");
	}


	//
	// Step 10: Client 2 saves a copy of emails into the local inbox directory
	//
	for (int i = 0; i < lastinboxresponsep->newemails; i++) {
		Msg emailMsg;
		Email *emailp = (Email *)emailMsg.buffer;

		if (msg_recv(sock, &emailMsg) != emailMsg.length) {
			err_sys("Receive Req error,exit");
		}

		Msg attachmentMsg;
		Attachment *attachmentp = (Attachment *)attachmentMsg.buffer;

		if (emailp->hasAttachment) {
			unsigned int tempsize = 0;
			int chunksize = 0;

			do {
				if (msg_recv(sock, &attachmentMsg) != attachmentMsg.length) {
					err_sys("Receive Req error,exit");
				}

				tempsize += ATTACHMENT_LENGTH;
				chunksize = tempsize > attachmentp->size ? attachmentp->size - tempsize + ATTACHMENT_LENGTH : ATTACHMENT_LENGTH;

				std::ofstream frecout((std::string)("Data\\") + "inbox\\" + get_email_filename(emailp) + "." + attachmentp->attachmentExtension,
					std::ios::out | std::ios::binary | std::ios::app);

				frecout.write((char*)&attachmentp->data[0], chunksize);
				frecout.close();
			} while (tempsize < attachmentp->size);

		}

		save_email((std::string)("Data\\") + "inbox", emailp, attachmentp);


		char end_time_str[32];
		ctime_s(end_time_str, sizeof(end_time_str), &emailp->timestamp);
		std::string end_time_string = end_time_str;
		end_time_string = end_time_string.substr(0, end_time_string.find('\n'));

		std::cout << "New email at " << (std::string)(end_time_string) << ": " << std::endl;
		std::cout << "   FROM: " << emailp->fromaddress << std::endl;
		std::cout << "     TO: " << emailp->toaddresses << std::endl;
		std::cout << "SUBJECT: " << emailp->subject << std::endl;
		std::cout << "   BODY: " << std::endl << emailp->body << std::endl;
	}


	// Close the client socket
	closesocket(sock);
}

TcpClient::~TcpClient() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}


unsigned long TcpClient::ResolveName(char name[]) {
	struct hostent *host;            /* Structure containing host information */

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}

/*
 * msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
 */
int TcpClient::msg_recv(int sock, Msg * msg_ptr) {
	unsigned int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}

/*
 * msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int TcpClient::msg_send(int sock, Msg * msg_ptr) {
	int n;
	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n - MSGHDRSIZE);

}

int main() {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD  mode;
	GetConsoleMode(hConsole, &mode);
	mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	SetConsoleMode(hConsole, mode);

	std::string bB = "\x1B[1m";
	std::string bUL = "\x1B[4m";
	std::string eS = "\x1B[24m";

	TcpClient *tc = new TcpClient();

	std::string input;
	do {
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		std::cout << "Do you want to ";
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN);
		std::cout << bUL << "s" << eS << "end";
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		std::cout << ", ";
		SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_BLUE);
		std::cout << bUL << "r" << eS << "eceive";
		SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		std::cout << ", or ";
		std::cout << bUL << "e" << eS << "xit? ";

		std::cin >> input;
		std::transform(input.begin(), input.end(), input.begin(), ::tolower);
		if (input == "s" || input == "send") {
			SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN);
			tc->runSender();
		} else if (input == "r" || input == "receive") {
			SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_BLUE);
			tc->runReceiver();
		}


		//
		// Step 11: Client lists emails in inbox/sent directories
		//
		if (input != "e" && input != "exit") {
			print_folder((std::string)("Data\\") + "inbox", (std::string)"INBOX");
			print_folder((std::string)("Data\\") + "sent", (std::string)"SENT");
		}

		SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	} while (input != "e" && input != "exit");

	system("pause");
	return 0;
}
