
#include <winsock.h>
#include <iostream.h>
#include <windows.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#define HOSTNAME_LENGTH 20
#define RESP_LENGTH 40
#define FILENAME_LENGTH 20
#define REQUEST_PORT 5001
#define BUFFER_LENGTH 1024 
#define TRACE 0
#define MAXPENDING 10
#define MSGHDRSIZE 8

typedef enum{
	REQ_SIZE=1,REQ_TIME,RESP //Message type
} Type;

typedef struct  
{
	char hostname[HOSTNAME_LENGTH];
	char filename[FILENAME_LENGTH];
} Req;  //request

typedef struct  
{
	char response[RESP_LENGTH];
} Resp; //response


typedef struct 
{
	Type type;
	int  length; //length of effective bytes in the buffer
	char buffer[BUFFER_LENGTH];
} Msg; //message format used for sending and receiving


int msg_recv(int ,Msg * );
int msg_send(int ,Msg * );
unsigned long ResolveName(char name[]);
void err_sys(char * fmt,...);


int main(void)
{
	Req * reqp; //a pointer to the Request Packet
	Resp resp;//response
	Msg smsg,rmsg; //send_message receive_message
	WSADATA wsadata;
	int serverSock,clientSock;     /* Socket descriptor for server and client,file handle*/
	struct sockaddr_in ServerAddr; /* Local address */
	struct sockaddr_in ClientAddr; /* Client address */
	unsigned short ServerPort;     /* Server port */
	int clientLen;            /* Length of client address data structure */
	char ser_name[HOSTNAME_LENGTH];
	///////////////////////////////////////////
	struct _stat stat_buf;
    int result;
	
	//initilize winsocket
	if (WSAStartup(0x0202,&wsadata)!=0)
		err_sys("Error in starting WSAStartup()\n");
	
	////Display name of local host
	if(gethostname(ser_name,HOSTNAME_LENGTH)!=0) //get the hostname
		err_sys("can not get the host name,program exit");
	
	printf("Server starting at host:%s\n",ser_name);
	printf("waiting to be contacted for time/size request...\n");
	
	
	//Create the server socket
	if ((serverSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		err_sys("Can not create the SERVER socket,exit");
	
	//Fill-in Server Port and Address info.
	ServerPort=REQUEST_PORT;
	memset(&ServerAddr, 0, sizeof(ServerAddr));   /* Zero out structure */
	ServerAddr.sin_family = AF_INET;                /* Internet address family */
	ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
	ServerAddr.sin_port = htons(ServerPort);      /* Local port */
	
	//Bind the server socket
    if (bind(serverSock, (struct sockaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
		err_sys("Can not create the SERVER socket,exit");
	
	//Successfull bind, now listen for client requests.
	if (listen(serverSock, MAXPENDING) < 0)
		err_sys("Can not create the SERVER socket,exit");
	
	for (;;) /* Run forever */
	{
		/* Set the size of the in-out parameter */
		clientLen = sizeof(ClientAddr);
	
		/* Wait for a client to connect */
		if ((clientSock = accept(serverSock, (struct sockaddr *) &ClientAddr, 
			&clientLen)) < 0)
			err_sys("Accept Failed ,exit");
		printf("Handling request from client %s\n", inet_ntoa(ClientAddr.sin_addr));

		//receive the request from client 
		if(msg_recv(clientSock,&rmsg)!=rmsg.length)
			err_sys("recv Req error,exit");
		
		//cast it to the request packet structure		
		reqp=(Req *)rmsg.buffer;
		printf("Requesting client name is:%s\n",reqp->hostname);
		//contruct the response and send it out
		smsg.type=RESP;
		smsg.length=sizeof(Resp);
		
		if((result = _stat(reqp->filename,&stat_buf))!=0)
			sprintf(resp.response,"No such a file");
		else {	
			memset(resp.response,0,sizeof(resp));
			if(rmsg.type==REQ_TIME)
			      sprintf(resp.response,"%s", ctime(&stat_buf.st_ctime ));
			else if(rmsg.type==REQ_SIZE)
			      sprintf(resp.response,"File size:%ld",stat_buf.st_size );
        }		

		memcpy(smsg.buffer,&resp,sizeof(resp));

		if(msg_send(clientSock,&smsg)!=smsg.length)
			err_sys("send Respose failed,exit");
 		printf("Response for %s has been sent out\n\n\n",reqp->hostname);

        closesocket(clientSock);
		printf("Waiting for anohter request ..............\n");
	}
	/* When done uninstall winsock.dll (WSACleanup()) and exit */ 
	WSACleanup();
	return 0;
}



void err_sys(char * fmt,...)
{     
	va_list args;
	perror(NULL);
	va_start(args,fmt);
	fprintf(stderr,"error: ");
	vfprintf(stderr,fmt,args);
	fprintf(stderr,"\n");
	va_end(args);
	exit(1);
}
unsigned long ResolveName(char name[])
{
	struct hostent *host;            /* Structure containing host information */
	
	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");
	
	/* Return the binary, network byte ordered address */
	return *((unsigned long *) host->h_addr_list[0]);
}

/*
msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
*/
int msg_recv(int sock,Msg * msg_ptr)
{
	int rbytes,n;
	
	for(rbytes=0;rbytes<MSGHDRSIZE;rbytes+=n)
		if((n=recv(sock,(char *)msg_ptr+rbytes,MSGHDRSIZE-rbytes,0))<=0)
			err_sys("Recv MSGHDR Error");
	
	for(rbytes=0;rbytes<msg_ptr->length;rbytes+=n)
		if((n=recv(sock,(char *)msg_ptr->buffer+rbytes,msg_ptr->length-rbytes,0))<=0)
			err_sys( "Recevier Buffer Error");
	return msg_ptr->length;
}

/* msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
 */
int msg_send(int sock,Msg * msg_ptr)
{
	int n;
	if((n=send(sock,(char *)msg_ptr,MSGHDRSIZE+msg_ptr->length,0))!=(MSGHDRSIZE+msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n-MSGHDRSIZE);
	
}
