/*
 * NEW: added this pragma to stop the security warnings
 */
 //#pragma warning(disable:4996)

#include <winsock.h>
#include <iostream>
#include <windows.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include "Thread.h"
#include "server.h"
#include <regex>
#include <chrono>
#include <ctime>


 /*
  * NEW: listen to ctrl c signal, to be used to exit the infinite listening loop
  */




  /*
   * NEW: email validation function, from https://stackoverflow.com/a/36904095
   */
bool is_email_valid(const std::string& email) {
	const std::regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
	return std::regex_match(email, pattern);
}


TcpServer::TcpServer() {
	WSADATA wsadata;
	if (WSAStartup(0x0202, &wsadata) != 0)
		TcpThread::err_sys("Starting WSAStartup() error\n");

	//Display name of local host
	if (gethostname(servername, HOSTNAME_LENGTH) != 0) //get the hostname
		TcpThread::err_sys("Get the host name error,exit");

	//printf("Server: %s waiting to be contacted for time/size request...\n",servername);
	printf("Mail Server Starting at host: %s\nwaiting to be contacted for transferring Mail...\n", servername);

	//Create the server socket
	if ((serverSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		TcpThread::err_sys("Create socket error,exit");

	//Fill-in Server Port and Address info.
	ServerPort = REQUEST_PORT;
	memset(&ServerAddr, 0, sizeof(ServerAddr));      /* Zero out structure */
	ServerAddr.sin_family = AF_INET;                 /* Internet address family */
	ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
	ServerAddr.sin_port = htons(ServerPort);         /* Local port */

	//Bind the server socket
	if (bind(serverSock, (struct sockaddr *) &ServerAddr, sizeof(ServerAddr)) < 0)
		TcpThread::err_sys("Bind socket error,exit");

	//Successfull bind, now listen for Server requests.
	if (listen(serverSock, MAXPENDING) < 0)
		TcpThread::err_sys("Listen socket error,exit");
}

TcpServer::~TcpServer() {
	/* When done uninstall winsock.dll (WSACleanup()) and exit */
	WSACleanup();
}



void TcpServer::start() {
	while (true) {
		/* Set the size of the result-value parameter */
		clientLen = sizeof(ClientAddr);

		/* Wait for a Server to connect */
		if ((clientSock = accept(serverSock, (struct sockaddr *) &ClientAddr, &clientLen)) < 0) {
			TcpThread::err_sys("Accept Failed ,exit");
		}

		/* Create a thread for this new connection and run */
		TcpThread * pt = new TcpThread(clientSock);
		pt->start();
	}
}



//////////////////////////////TcpThread Class //////////////////////////////////////////
void TcpThread::err_sys(const char *fmt, ...) {
	perror(NULL);
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "error: ");
	vfprintf(stderr, fmt, args);
	fprintf(stderr, "\n");
	va_end(args);
	exit(1);
}

unsigned long TcpThread::ResolveName(char name[]) {
	struct hostent *host;            /* Structure containing host information */

	if ((host = gethostbyname(name)) == NULL)
		err_sys("gethostbyname() failed");

	/* Return the binary, network byte ordered address */
	return *((unsigned long *)host->h_addr_list[0]);
}

/*
msg_recv returns the length of bytes in the msg_ptr->buffer,which have been recevied successfully.
*/
int TcpThread::msg_recv(int sock, Msg * msg_ptr) {
	int rbytes, n;

	for (rbytes = 0; rbytes < MSGHDRSIZE; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr + rbytes, MSGHDRSIZE - rbytes, 0)) <= 0)
			err_sys("Recv MSGHDR Error");

	for (rbytes = 0; rbytes < msg_ptr->length; rbytes += n)
		if ((n = recv(sock, (char *)msg_ptr->buffer + rbytes, msg_ptr->length - rbytes, 0)) <= 0)
			err_sys("Recevier Buffer Error");

	return msg_ptr->length;
}

/* msg_send returns the length of bytes in msg_ptr->buffer,which have been sent out successfully
*/
int TcpThread::msg_send(int sock, Msg * msg_ptr) {
	int n;
	if ((n = send(sock, (char *)msg_ptr, MSGHDRSIZE + msg_ptr->length, 0)) != (MSGHDRSIZE + msg_ptr->length))
		err_sys("Send MSGHDRSIZE+length Error");
	return (n - MSGHDRSIZE);

}

void TcpThread::run() { //cs: Server socket
	Resp * respp;//a pointer to response
	Req * reqp; //a pointer to the Request Packet
	Msg smsg, rmsg; //send_message receive_message
	//struct _stat stat_buf;
	//int result;

	if (msg_recv(cs, &rmsg) != rmsg.length)
		err_sys("Receive Req error,exit");

	// Cast the response message to the request packet structure		
	reqp = (Req *)rmsg.buffer;

	auto current = std::chrono::system_clock::now();
	std::time_t current_time = std::chrono::system_clock::to_time_t(current);

	printf("Mail Received from %s\n", reqp->clienthostname);
	printf("FROM: %s\n", reqp->fromhostname);
	printf("TO: %s\n", reqp->tohostname);
	printf("SUBJECT: %s\n", reqp->subject);
	printf("TIME: %s\n", reqp->timestamp);
	printf("%s\n", reqp->body);


	//construct the response and send it out 
	smsg.length = sizeof(Resp);
	respp = (Resp *)smsg.buffer;
	memset(respp->response, 0, sizeof(Resp));
	if (is_email_valid(reqp->fromhostname) && is_email_valid(reqp->tohostname)) {
		sprintf_s(respp->response, "250 OK");
	} else {
		sprintf_s(respp->response, "501 Error");
	}
	char current_time_str[30];
	ctime_s(current_time_str, sizeof(current_time_str), &current_time);
	sprintf_s(respp->timestamp, "%s", current_time_str);

	//if ((result  = _stat(reqp->fromhostname, &stat_buf)) != 0)
	//	sprintf(respp->response, "No such a file");
	//else {
	//	memset(respp->response, 0, sizeof(Resp));
	//	if (rmsg.type == REQ_TIME)
	//		sprintf(respp->response, "%s", ctime(&stat_buf.st_ctime));
	//	else if (rmsg.type == REQ_SIZE)
	//		sprintf(respp->response, "File size:%ld", stat_buf.st_size);
	//}

	if (msg_send(cs, &smsg) != smsg.length)
		err_sys("send Respose failed,exit");
	printf("Response for %s has been sent out \n\n\n", reqp->clienthostname);

	closesocket(cs);
}



////////////////////////////////////////////////////////////////////////////////////////

int main(void) {
	TcpServer ts;
	ts.start();

	system("pause");
	return 0;
}


