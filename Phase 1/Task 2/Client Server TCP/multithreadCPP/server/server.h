#ifndef SER_TCP_H
#define SER_TCP_H

/*
 * NEW: added SUBJECT_LENGTH, BODY_LENGTH, and commented FILENAME_LENGTH
 */
#define HOSTNAME_LENGTH 20
#define SUBJECT_LENGTH 20
#define TIMESTAMP_LENGTH 30
#define BODY_LENGTH 500
 //#define FILENAME_LENGTH 20
#define RESP_LENGTH 40
#define BUFFER_LENGTH 1024
#define REQUEST_PORT 5001
#define MAXPENDING 10
#define MSGHDRSIZE 4 // message header size (4 bytes for length)

/*
 * NEW: no longer using message request/response type
 */
 //typedef enum {
 //	REQ_SIZE = 1,
 //	REQ_TIME,
 //	RESP // Message type
 //} Type;

typedef struct {
	char clienthostname[HOSTNAME_LENGTH];


	char tohostname[HOSTNAME_LENGTH];
	char fromhostname[HOSTNAME_LENGTH];
	char subject[SUBJECT_LENGTH];
	char timestamp[TIMESTAMP_LENGTH];

	char body[BODY_LENGTH];
	//char filename[FILENAME_LENGTH];
} Req;  //request

typedef struct {
	char response[RESP_LENGTH];
	char timestamp[TIMESTAMP_LENGTH];
} Resp; //response


typedef struct {
	int  length; // length of effective bytes in the buffer
	char buffer[BUFFER_LENGTH];
} Msg; //message format used for sending and receiving


class TcpServer {
	int serverSock;				      // socket descriptor for server
	int clientSock;					  // socket descriptor for client
	struct sockaddr_in ServerAddr;    // address of server
	struct sockaddr_in ClientAddr;    // address of client
	unsigned short ServerPort;        // server port
	int clientLen;				      // length of server address data structure
	char servername[HOSTNAME_LENGTH];

public:
	TcpServer();
	~TcpServer();
	void start();
};

class TcpThread : public Thread {
	int cs;

public:
	TcpThread(int clientsocket) :cs(clientsocket) {
	}
	virtual void run();
	int msg_recv(int, Msg *);
	int msg_send(int, Msg *);
	unsigned long ResolveName(char name[]);
	static void err_sys(const char * fmt, ...);
};

#endif